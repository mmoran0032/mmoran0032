# mmoran0032

[![PyPI](https://img.shields.io/pypi/v/mmoran0032)](https://pypi.org/project/mmoran0032/)

`whoami` but for me and within Python for all to see.

## Install

`python -m pip install mmoran0032`

## Run

`mmoran0032`

## Contact

Please reach out on any of the provided channels.
